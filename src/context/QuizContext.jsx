import { createContext, useContext, useEffect, useReducer } from "react";

const QuizContext = createContext();

const SECONDS_PER_QUESTIONS = 20;

const initialState = {
  questions: [],
  // 'loading', 'error', 'ready', 'active', 'finished'
  status: "loading",
  index: 0,
  answer: null,
  points: 0,
  record: 0,
};

function reducer(state, action) {
  switch (action.type) {
    case "dataReceived":
      return (state = { ...state, questions: action.payload, status: "ready" });
    case "dataFailed":
      return (state = { ...state, status: "error" });
    case "start":
      return { ...state, status: "active" };
    case "answer": {
      console.log(action.payload);
      return { ...state, answer: action.payload };
    }
    case "newAnswer": {
      return {
        ...state,
        index: state.index + 1,
        answer: null,
        points:
          state.answer == state.questions[state.index].correctOption
            ? state.points + state.questions[state.index].points
            : state.points,
      };
    }
    case "finish":
      return {
        ...state,
        record: state.points > state.record ? state.points : state.record,
        status: "finish",
      };
    case "restart": {
      return { ...state, status: "ready", index: 0, answer: null, points: 0 };
    }
    default:
      throw new Error("No such action");
  }
}

const CustomProvider = ({ children }) => {
  const [{ questions, status, index, answer, points, record }, dispatch] =
    useReducer(reducer, initialState);

  const numberOfQuestions = questions.length;
  const totalQuestionsPoint =
    questions.length > 0 &&
    questions.reduce((calc, b) => (calc += b.points), 0);

  useEffect(() => {
    fetch(`http://localhost:8080/questions`)
      .then((res) => res.json())
      .then((data) => dispatch({ type: "dataReceived", payload: data }))
      .catch((error) => dispatch({ type: "dataFailed" }));
  }, []);

  return (
    <QuizContext.Provider
      value={{
        record,
        questions,
        index,
        status,
        answer,
        points,
        numberOfQuestions,
        totalQuestionsPoint,
        SECONDS_PER_QUESTIONS,
        dispatch,
      }}
    >
      {children}
    </QuizContext.Provider>
  );
};

export default CustomProvider;

export const useQuiz = () => useContext(QuizContext);
