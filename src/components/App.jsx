import Header from "./Header";
import MainWrap from "./MainWrap";
import Loader from "./Loader";
import Error from "./Error";
import StartScreen from "./StartScreen";
import Questions from "./Questions";
import NextButton from "./NextButton";
import Progress from "./Progress";
import FinishScreen from "./FinishScreen";
import Timer from "./Timer";
import Footer from "./Footer";
import { useQuiz } from "../context/QuizContext";

function App() {
  const {
    questions,
    index,
    status,
    answer,
    points,
    numberOfQuestions,
    totalQuestionsPoint,
    SECONDS_PER_QUESTIONS,
    dispatch,
  } = useQuiz(); //custom hook

  return (
    <div className="app">
      <Header />
      <MainWrap>
        {status === "loading" && <Loader />}
        {status === "error" && <Error />}
        {status === "ready" && <StartScreen />}
        {status === "active" && (
          <>
            <Progress
              index={index}
              numberOfQuestions={numberOfQuestions}
              points={points}
              totalQuestionsPoint={totalQuestionsPoint}
            />
            <Questions
              question={questions[index]}
              dispatch={dispatch}
              answer={answer}
            />
            <Footer>
              <Timer
                numberOfQuestions={numberOfQuestions}
                time={SECONDS_PER_QUESTIONS}
              />
              <NextButton
                dispatch={dispatch}
                answer={answer}
                index={index}
                numberOfQuestions={numberOfQuestions}
              />
            </Footer>
          </>
        )}
        {status === "finish" && <FinishScreen/>}
      </MainWrap>
    </div>
  );
}

export default App;
