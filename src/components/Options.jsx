import React from "react";

const Options = ({ options, dispatch, answer, correctOption }) => {
  const hasAnswer = answer !== null;

  return (
    <div className="options">
      {options.map((option, index) => {
        return (
          <button
            className={`btn btn-option ${answer == index ? "answer" : ""}  ${
              hasAnswer ? (correctOption == index ? "correct" : "wrong") : ""
            }`}
            onClick={() => dispatch({ type: "answer", payload: index })}
            disabled={hasAnswer}
            key={index}
          >
            {option}
          </button>
        );
      })}
    </div>
  );
};
export default Options;
