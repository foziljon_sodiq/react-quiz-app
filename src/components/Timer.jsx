import React, { useEffect, useState } from "react";

const Timer = ({ time, numberOfQuestions }) => {
  const totalSeconds = time * numberOfQuestions;
  const [second, setSecond] = useState(totalSeconds);
  let minute = Math.floor(second / 60);
  let leftSecond = second % 60;
  useEffect(() => {
    const timer = setInterval(() => {
      setSecond((prev) => (prev -= 1));
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }, [second]);

  return (
    <button className="btn btn-ui timer">
      {minute} : {leftSecond}
    </button>
  );
};

export default Timer;
