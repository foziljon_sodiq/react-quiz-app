import React from "react";

const Progress = ({
  index,
  numberOfQuestions,
  points,
  totalQuestionsPoint,
}) => {
  return (
    <>
      <progress value={index} max={numberOfQuestions} />
      <header className="progress">
        <p>
          {index + 1} / {numberOfQuestions}
        </p>
        <p>
          {points} / {totalQuestionsPoint}
        </p>
      </header>
    </>
  );
};

export default Progress;
