import React from "react";
import { useQuiz } from "../context/QuizContext";

const FinishScreen = () => {
  const { points, totalQuestionsPoint, dispatch, record } = useQuiz();

  const percentagePoints = Math.floor((100 * points) / totalQuestionsPoint);
  let resultEmo = "";
  if (percentagePoints <= 20) resultEmo = "👎";
  else if (percentagePoints <= 40) resultEmo = "😒";
  else if (percentagePoints <= 60) resultEmo = "😉";
  else if (percentagePoints <= 80) resultEmo = "😃";
  else if (percentagePoints >= 80) resultEmo = "😍😎";

  return (
    <>
      <div className="result">
        {resultEmo}
        Siz {totalQuestionsPoint} balldan {points} ball topladingiz ({" "}
        {percentagePoints}% )
      </div>

      <button className="btn  left">Record: {record}</button>
      <button
        className="btn btn-ui"
        onClick={() => dispatch({ type: "restart" })}
      >
        Restart
      </button>
    </>
  );
};

export default FinishScreen;
