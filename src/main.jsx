import React from "react";
import ReactDOM from "react-dom/client";
import App from "./components/App.jsx";
import "./index.css";
import CustomProvider from "./context/QuizContext.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <CustomProvider>
    <App />
  </CustomProvider>
);
